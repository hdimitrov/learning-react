import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/store';
import App from './App';
import * as serviceWorker from './serviceWorker';
import listenToSocket from './outside/listenToSocket';
import initialiseNumber from './outside/initialiseNumber';

import './index.css'; // this shouldnt ◘be here but i dont wanna move it

const store = configureStore();
listenToSocket(store);
initialiseNumber(store);
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
