import React from 'react';
import Enzyme from 'enzyme';
import axios from 'axios';
import {addNumber} from './store/actions/actions';
import Adapter from 'enzyme-adapter-react-16';
import numbers from './store/reducers/numbers';
import SaveButton from './components/SaveButton';
import {NumberAdder} from './containers/NumberAdder';
Enzyme.configure({ adapter: new Adapter() });

describe('Test Button Click', () => {
	it('triggers method', () => {
		const methodToCall = jest.fn();
	   const wrapper = Enzyme.shallow(<SaveButton onClick={methodToCall}/>);
	   const component = wrapper.shallow();
	   component.find('input').simulate('click');
	   expect(methodToCall.mock.calls.length).toEqual(1);
	 });
});

describe('Test store stuff', () => {
	it('triggers method', () => {
		const params = {
			showNotification: false,
			dismiss: jest.fn(),
			addNumber: jest.fn(),
			triggerTopNotificationIfNecessary: jest.fn(),
		}
		const wrapper = Enzyme.shallow(<NumberAdder {...params}/>);
		wrapper.setState({input: 15});
		console.log(wrapper.getElement());
		wrapper.instance().addNumber();
		expect(params.addNumber.mock.calls[0][0]).toEqual(15);
		expect(params.triggerTopNotificationIfNecessary.mock.calls[0][0]).toEqual(15);
	 });
});


describe('Test action', () => {
	it('triggers method', async () => {
		expect(addNumber(5)).toMatchSnapshot();
		expect(axios.post.mock.calls).toMatchSnapshot();

	 });
});

describe('Test reducer', () => {
	it('triggers method', async () => {
		const initialState = {numbers: [1]};
		expect(numbers(initialState, {type: 'addNumber', payload: 5})).toMatchSnapshot();
	});
	
});