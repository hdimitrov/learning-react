import socket from 'socket.io-client';
import { addNumber } from '../store/actions/actions';
const listenToSocket = (store) => {
	const io = socket('http://localhost:3001');
	io.on('connect', () => {
		io.on('addNumber', (number) => {
			store.dispatch(addNumber(number));
		});
	});
};

export default listenToSocket;
