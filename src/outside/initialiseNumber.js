import { addNumber } from '../store/actions/actions';
import axios from 'axios';
const initialiseNumber = (store) => {
	axios.get('http://localhost:3001/api/numbers').then((rs) => store.dispatch(addNumber(rs.data.total, false)));
};

export default initialiseNumber;
