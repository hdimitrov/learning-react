import axios from 'axios';

export const addNumber = (number, sendToServer = true) => {
	if (sendToServer) {
		axios.post('http://localhost:3001/api/numbers', { number });
	}

	return {
		type: 'addNumber',
		payload: number,
	};
};

export const triggerTopNotificationIfNecessary = (number) => {
	return (dispatch) => {
		axios.get('http://localhost:3001/api/numbers/top')
			.then((result) => {
				const top = result.data.number || 0;
				console.log(top);
				if (number >= top) {
					return dispatch({
						type: 'toggleTop',
					});
				}
			});
	};
};

export const dismiss = () => ({type: 'toggleTop'});