import { combineReducers } from 'redux';
import numbers from './numbers';
import notification from './notification';

export default combineReducers({
 numbers,
 notification
});