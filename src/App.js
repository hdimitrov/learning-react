import React from 'react';
import NumberAdder from './containers/NumberAdder';
import NumberShow from './containers/NumberShow';
import './App.css';

function App() {
	return (
		<div className="App">
			<NumberAdder />
			<NumberShow />
		</div>
	);
}

export default App;