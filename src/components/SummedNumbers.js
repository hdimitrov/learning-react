import React from 'react';
import PropTypes from 'prop-types';

const SummedNumbers = ({ sum }) => (
	<h1>{ sum }</h1>
);

SummedNumbers.propTypes = {
	sum: PropTypes.number.isRequired,
};

export default SummedNumbers;
