import React, { Component } from 'react';
import PropTypes from 'prop-types'

class CongratulationsPopup extends Component {

    static propTypes = {
        dismiss: PropTypes.func.isRequired,
        showNotification: PropTypes.bool.isRequired,

    };
    handleError = (input) => {
        return  !/^[0-9]*$/.test(input);
    };
    render() {
        if(this.props.showNotification) {
            return (
                <div>
                    CONGRATULATIONS! <input type="submit" onClick={this.props.dismiss} value="dismiss" />
                </div>
            );
        }
        return (null);
	}
}


export default CongratulationsPopup;
