import React from 'react'
import PropTypes from 'prop-types'
import style from  './RandomButton.module.css';
const RandomButton = ({onClick}) => (
  <input type="submit" className={style.randomBtn} value="randomValue" onClick={onClick}></input>
);

RandomButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};

export default RandomButton;