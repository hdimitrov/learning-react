import React, { Component } from 'react';
import PropTypes from 'prop-types'

class NumberInput extends Component {
    state = {
        input: '',
    };
    static propTypes = {
        onChange: PropTypes.func.isRequired,
    };
    handleError = (input) => {
        return  !/^[0-9]*$/.test(input);
    };
    render() {
        return (
            <input type="text" value={this.state.input} onChange={(ev) => {
                if (!this.handleError(ev.target.value)) {
                    this.props.onChange(ev);
                    this.setState({ input:ev.target.value });
                }
            }}></input>
		);
	}
}


export default NumberInput;
