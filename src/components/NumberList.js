import React from 'react';
import PropTypes from 'prop-types';
import NumberListItem from './NumberListItem';

const NumberList = ({ numbers }) => {
	return numbers.map((number, key) => {
		return <NumberListItem key={key} number={number}/>
	})
};

NumberList.propTypes = {
	numbers: PropTypes.array.isRequired,
};

export default NumberList;
