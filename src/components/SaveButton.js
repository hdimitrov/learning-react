import React from 'react'
import PropTypes from 'prop-types'

const SaveButton = ({onClick}) => (
  <input type="submit" onClick={onClick}></input>
);

SaveButton.propTypes = {
    onClick: PropTypes.func.isRequired,
};

export default SaveButton;