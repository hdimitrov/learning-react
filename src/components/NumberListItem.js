import React from 'react';
import PropTypes from 'prop-types';

const NumberListItem = ({ number }) => (
	<h2>{ number }</h2>
);

NumberListItem.propTypes = {
	number: PropTypes.number.isRequired,
};

export default NumberListItem;
