import React, { Component } from 'react';
import SaveButton from '../components/SaveButton';
import NumberInput from '../components/NumberInput';
import RandomButton from '../components/RandomButton';
import CongratulationsPopup from '../components/CongratulationsPopup';

import {connect} from 'react-redux';
import {addNumber, triggerTopNotificationIfNecessary, dismiss} from '../store/actions/actions';
export class NumberAdder extends Component {
    state = {
        input: '',
    };
    constructor(props) {
        super(props);
        this.numberInput = React.createRef();
    }
    onChangeInput = (input) =>  {
        this.setState({input: input.target.value});
    };
    addNumber = () => {
        this.props.addNumber(this.state.input);
        this.props.triggerTopNotificationIfNecessary(this.state.input);
        if(this.numberInput.current) {
            // if correctly mounted child
            this.numberInput.current.setState({input: ''});
        }
    };
    generateRandomNumber = () => {
        let num = Math.floor((Math.random() * 100));
        this.props.addNumber(num);
    };
    render() {
        return (
            <div>
                <CongratulationsPopup showNotification={this.props.showNotification} dismiss={this.props.dismiss}/>
                <NumberInput ref={this.numberInput} onChange={this.onChangeInput}/> 
                <SaveButton onClick={this.addNumber}/> 
                <RandomButton onClick={this.generateRandomNumber} />
            </div>
        );
    }
}


function mapDispatchToProps(dispatch) {
  return {
      addNumber: (number) => dispatch(addNumber(number)),
      triggerTopNotificationIfNecessary: (number) => dispatch(triggerTopNotificationIfNecessary(number)),
      dismiss: () => dispatch(dismiss()),
  }
}

function mapStateToProps(state) {
    return {
        showNotification: state.notification.showTop,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NumberAdder);
