import React, { Component } from 'react';
import {connect} from 'react-redux';
import SummedNumbers from '../components/SummedNumbers';
import NumberList from '../components/NumberList';
import PropTypes from 'prop-types'

class NumberAdder extends Component {
	static propTypes = {
		sum: PropTypes.number.isRequired,
		numbers: PropTypes.array.isRequired,

	};
	render() {
		return (
			<div>
				<SummedNumbers sum={this.props.sum}/>
                <NumberList numbers={this.props.numbers}/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
        sum: state.numbers.numbers.reduce((acc, curr) => acc += parseInt(curr, 10), 0),
        numbers: state.numbers.numbers.map((entry) => parseInt(entry, 10)),
	};
}

export default connect(mapStateToProps)(NumberAdder);
